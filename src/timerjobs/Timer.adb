package body timer is

   protected body ActualTime is
      entry AddMinute
        when True is
      begin
         if MinutesSingle < 9 then
            MinutesSingle := MinutesSingle + 1;
         else
            MinutesSingle := 0;
            if MinutesDecimal < 5 then
               MinutesDecimal := MinutesDecimal + 1;
            else
               MinutesDecimal := 0;
               if HoursDecimal < 2 and HoursSingle < 9 then
                  HoursSingle := HoursSingle + 1;
               else
                  if HoursDecimal < 2 then
                     HoursDecimal := HoursDecimal + 1;
                     HoursSingle := 0;
                  else
                     if HoursSingle < 3 then
                        HoursSingle := HoursSingle + 1;
                     else
                        HoursSingle := 0;
                        HoursDecimal := 0;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end AddMinute;

      entry GetActualTime (time : out TimeString)
        when True is
      begin
         time := Integer'Image(HoursDecimal)
           & Integer'Image(HoursSingle)
           &":"
           & Integer'Image(MinutesDecimal)
           & to_unbounded_string(Integer'Image(MinutesSingle));

      end GetActualTime;
   end ActualTime;

   task body TimerJob is
   begin
      loop
         commonActualTime.AddMinute;
         delay Duration(0.01);
      end loop;
   end TimerJob;
end timer;
