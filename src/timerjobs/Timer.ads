with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Text_IO; use Ada.Strings.Unbounded.Text_IO;

package timer is

   subtype TimeString is Unbounded_String;

   protected type ActualTime is
      entry GetActualTime (time : out TimeString);
      entry AddMinute;
   private
      HoursSingle : Integer := 0;
      HoursDecimal : Integer := 0;
      MinutesSingle : Integer := 0;
      MinutesDecimal : Integer := 0;
   end ActualTime;

   type ActualTimePointer is access ActualTime;

   task type TimerJob (commonActualTime : ActualTimePointer);
   type TimerJobPointer is access TimerJob;

end timer;
