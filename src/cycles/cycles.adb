package body cycles is

   task body VehicleCycle is
      actualHour : TimeString;
      tempMessage : Unbounded_String;
      cycleLogger : LogHelperAccess;
      logName : Unbounded_String;
   begin
      logName := To_Unbounded_String("");
      Append(logName, "cycle_");
      Append(logName, Integer'Image(parameters.VehicleNumber));
      Append(logName, ".log");
      cycleLogger := new LogHelper;
      cycleLogger.SetLogName(renamedLogName => logName);
      loop
         commonActualTime.GetActualTime(actualHour);
         tempMessage := To_Unbounded_String("");
         Append(tempMessage, "Vehicle on track: ");
         Append(tempMessage, Integer'Image(parameters.VehicleNumber));
         Append(tempMessage, " ");
         Append(tempMessage, actualHour);

         cycleLogger.Log(message => tempMessage);
         delay Duration(parameters.Duration);
      end loop;
   end VehicleCycle;

end cycles;
