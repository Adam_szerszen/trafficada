with timer; use timer;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded.Text_IO; use Ada.Strings.Unbounded.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with logger; use logger;

package cycles is

   type CycleParams;
   type CycleParamsAccess is access CycleParams;
   type CycleParams is record
      Duration : Float;
      VehicleNumber : Integer;
   end record;

   task type VehicleCycle (commonActualTime : ActualTimePointer;
                           parameters : CycleParamsAccess
                          );
 --concreteVehicle : VehiclePointer;
   type VehicleCyclePointer is access VehicleCycle;

end cycles;
