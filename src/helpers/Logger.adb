package body logger is

   protected body LogHelper is
      procedure SetLogName (renamedLogName : FileNameString) is
      begin
         LogName := renamedLogName;
      end SetLogName;

      procedure Log (message :  MessageString) is
         use Ada.Text_IO;
      begin
         Open(File => F,
                Mode => Append_File,
                Name => To_String(LogName));
         WriteMessage(message => message);
      exception
         when Name_Error =>
            Create(File => F,
                   Mode => Out_File,
                   Name => To_String(LogName));
            WriteMessage(message => message);
      end Log;

      procedure WriteMessage(message : MessageString) is
         use Ada.Text_IO;
      begin
         Put_Line(F, message);
         Close(F);
      end WriteMessage;
   end LogHelper;

end logger;
