with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO.Unbounded_IO; use Ada.Text_IO.Unbounded_IO;

package logger is

   subtype MessageString is Unbounded_String;
   subtype FileNameString is Unbounded_String;

   protected type LogHelper is
      procedure Log(message :  MessageString);
      procedure SetLogName(renamedLogName : FileNameString);
   private
      LogName : FileNameString := To_Unbounded_String("my_log.log");
      F : Ada.Text_IO.File_Type;
      procedure WriteMessage(message : MessageString);
   end LogHelper;

   type LogHelperAccess is access LogHelper;

end logger;
