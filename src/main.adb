with Ada.Text_IO; use Ada.Text_IO;
with timer; use timer;
with cycles; use cycles;
with logger; use logger;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

procedure Main is
   MyTimer : ActualTimePointer := new ActualTime;
   ActualTime : TimeString;
   commonTimer : timer.TimerJobPointer := new timer.TimerJob(MyTimer);
   paramsOne : CycleParamsAccess := new CycleParams'(Duration => 0.8,
                                                     VehicleNumber => 1);
   paramsTwo : CycleParamsAccess := new CycleParams'(Duration => 1.5,
                                                     VehicleNumber => 2);
   paramsThree : CycleParamsAccess := new CycleParams'(Duration => 2.7,
                                                       VehicleNumber => 3);
   vehicleOne : cycles.VehicleCyclePointer :=
     new cycles.VehicleCycle(MyTimer, paramsOne);
   vehicleTwo : cycles.VehicleCyclePointer :=
     new cycles.VehicleCycle(MyTimer, paramsTwo);
   vehicleThree : cycles.VehicleCyclePointer :=
     new cycles.VehicleCycle(MyTimer, paramsThree);
begin
   Put_Line("Program begins...!");
end Main;
