with route; use route;

package vehicle is

   type Vehicle;
   type VehiclePointer is access Vehicle;
   type Vehicle is record

      Id : String (1.. 3);
      MaxVelocity : Integer;
      MaxCapacity : Integer;
      Route : DrivingRoute.Vector;
   end record;

   function CreateVehicle(id : in String;
                          maxVelocity : in Integer;
                          maxCapacity : in Integer)
                          return VehiclePointer;

end vehicle;
